import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;

/**
 * @author Gurjot Sandher, Gurjot Mander
 * @year 2021
 * @package PACKAGE_NAME
 *
 * Test case 3 runs a series of test to assure that the completed and pending buttons work appropriately.
 */

/*

Important

  The code is working and the cases are tested. However there's a bug where the complete button cannot
  be clicked. We must go into the DOM and somehow change it from being a 'btn-disabled' to being a
  'btn-success'. The method is currently being tested for it be a fail.

*/
    
public class TestCase3 {

    //Constant for quick access in testing.
    //Edit: I changed the constant naming style for good practice. Bob.
    /**  api website to run tests on. */
    public final static String TEST_WEBSITE = "http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html";

    /**  Date for test task 1. */
    public final static String TEST_DATE_1 = "01/25/2025";

    /**  Date for test task 2. */
    public final static String TEST_DATE_2 = "09/25/2025";

    /**  Name for test task 1. */
    public final static String TEST_TASK_1 = "Task 1";

    /**  Name for test task 2. */
    public final static String TEST_TASK_2 = "Task 2";

    /**  Constant for Chrome Driver. */
    public static ChromeDriver driver;

    /**  Constant for helper java class. */
    public static TaskListPage taskListPage;

    /**
     * Restarts the browser between each test.
     */
    @Before
    public void prep() throws InterruptedException {
        //Initialize test browser
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\SeleniumDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        taskListPage = new TaskListPage(driver);
        driver.get(TEST_WEBSITE);

        // Arrange our test case
        taskListPage.addTask(TEST_TASK_1, TEST_DATE_1, false);
        taskListPage.addTask(TEST_TASK_2, TEST_DATE_2, true);
    }

    /**
     * Tests the correct entries are made
     */
    @Test
    public void testCorrectEntries() {

        boolean status = false;

        try {
            String task1 = driver.findElement(By.xpath("//h3[contains(text(), '" + TEST_TASK_1 + "')]")).getText();
            String task2 = driver.findElement(By.xpath("//h3[contains(text(), '" + TEST_TASK_2 + "')]")).getText();

            // We might want to put a Thread.sleep() here or an async feature to let the tasks be added

            String pending = driver.findElement(By.xpath("//ul/li[1]/span/button[contains(text(), '" + "pending" + "')]")).getText();
            String complete = driver.findElement(By.xpath("//ul/li[2]/span/button[contains(text(), '" + "complete" + "')]")).getText();

            if ((task1.equalsIgnoreCase(TEST_TASK_1)) && (task2.equalsIgnoreCase(TEST_TASK_2)) && (pending.equalsIgnoreCase("pending")) && (complete.equalsIgnoreCase("complete"))) {
                status = true;
            }

        } catch (Exception ex) {

        } finally {
            Assert.assertTrue(status);

        }

    }

    /**
     * Test checks if task changes from pending to completed.
     */
    @Test
    public void testToComplete() {

        // The item that is pending is located and clicked after confirming it is of pending value
        String inputtedStr = "pending";

        String strPending = driver.findElement(By.xpath("//ul/li[1]/span/button[contains(text(), '" + inputtedStr + "')]")).getText();
        taskListPage.clickBtn(By.xpath("//ul/li[1]/span/button"));

        // The item is shown here to no long be "pending" and now is shown as "Completed"
        inputtedStr = "complete";
        String strCompleted = driver.findElement(By.xpath("//ul/li[1]/span/button[contains(text(), '" + inputtedStr + "')]")).getText();

        //We check that the string is now listed as "Completed" and the required change was completed.
        Assert.assertEquals(inputtedStr, strCompleted);
    }

    /**
     * Test checks if task changes from completed to pending.
     */
    @Test
    public void testToPending() {

        // The item that is pending is located and clicked after confirming it is of pending value

        // For status to be false and therefore provide a passing test
        // as we have made the expected value to be false.
        String inputtedStr = "complete";
        String expectedStr = "pending";

        boolean status = false;

        try {
            String foundString = driver.findElement(By.xpath("//ul/li[1]/span/button[contains(text(), '" + inputtedStr + "')]")).getText();
            taskListPage.clickBtn(By.xpath("//ul/li[1]/span/button"));
            driver.findElement(By.xpath("//ul/li[1]/span/button[contains(text(), '" + expectedStr + "')]"));
            status = true;

        } catch (org.openqa.selenium.NoSuchElementException e) {
            status = false;
        } finally {
            Assert.assertFalse(status);
        }

    }

    /**
     * Closes the Chrome Driver.
     */
    @After
    public void closeBrowser(){
        driver.quit();
    }

}
