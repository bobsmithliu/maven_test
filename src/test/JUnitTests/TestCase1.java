import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author Zichang Liu, Chang Sun, Gurjot Sandher
 * @year 2021
 * @package PACKAGE_NAME
 *
 * Test Case one runs a series of tests to assure that all the elements on the page are displayed.
 */
public class TestCase1 {

    /**  Constant for Chrome Driver. */
    public static WebDriver driver;

    /**  api website to run tests on. */
    public final static String TEST_WEBSITE = "http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html";

    /**  Constant for helper java class. */
    public static TaskListPage testTLP;

    /**
     * Runs once before all tests to open the browser and prep for the tests.
     */
    @BeforeClass
    public static void createPrep() {
        //uncomment to set driver within MacOS
//        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");

        // uncomment to set driver within WindowsOS
         System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\SeleniumDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(TEST_WEBSITE);
        testTLP = new TaskListPage(driver);
    }

    /**
     * Restarts the browser between each test.
     */
    @Before
    public void prep() {
        driver.navigate().refresh();
    }

    /**
     * Tests the Fortinet link is displayed.
     */
    @Test
    public void testFortinetLinkPresent()  {
        assertTrue(driver.findElement(testTLP.taskPageHomeLink).isDisplayed());
    }

    /**
     * Tests the task list title is displayed.
     */
    @Test
    public void testTaskListTitlePresent()  {
        assertTrue(driver.findElement(testTLP.title).isDisplayed());
    }

    /**
     * Tests the task name is displayed.
     */
    @Test
    public void testTaskNameFieldPresent() {
        assertTrue(driver.findElement(testTLP.taskNameInput).isDisplayed());
    }

    /**
     * Tests the task deadline date is displayed.
     */
    @Test
    public void testDeadlineFieldPresent() {
        assertTrue(driver.findElement(testTLP.deadlineInput).isDisplayed());
    }

    /**
     * Tests the task checkbox is displayed.
     */
    @Test
    public void testCompleteCheckBoxPresent() {
        assertTrue(driver.findElement(testTLP.completeCheckBox).isDisplayed());
    }

    /**
     * Tests the add item button is displayed.
     */
    @Test
    public void testAddItemButtonPresent() {
        assertTrue(driver.findElement(testTLP.addItemBtn).isDisplayed());
    }

    /**
     * Closes the Chrome Driver.
     */
    @AfterClass
    public static void closing() {
        driver.quit();
    }
}