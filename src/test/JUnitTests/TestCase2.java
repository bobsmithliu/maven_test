import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * @author Gurjot Sandher, Gurjot Mander
 * @year 2021
 * @package PACKAGE_NAME
 *
 * Test case 2 runs a series of tests to assure that tasks are added to the task list appropriately.
 */

public class TestCase2 {

    /**  Constant for Chrome Driver. */
    public static ChromeDriver driver;

    /**  Constant for helper java class. */
    public static TaskListPage taskListPage;

    /**  Constant for arraylist of web elements. */
    public static ArrayList<WebElement> list;

    /**  Date for test task 1. */
    public final static String TEST_DATE_1 = "01/25/2025";

    /**  Date for test task 2. */
    public final static String TEST_DATE_2 = "09/25/2025";

    /**  Name for test task 1. */
    public final static String TEST_TASK_1 = "Task 1";

    /**  Name for test task 2. */
    public final static String TEST_TASK_2 = "Task 2";

    /**  api website to run tests on. */
    public final static String TEST_WEBSITE = "http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html";

    /**
     * Restarts the browser between each test.
     */
    @Before
    public void prep() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\SeleniumDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(TEST_WEBSITE);
        taskListPage = new TaskListPage(driver);

        taskListPage.addTask(TEST_TASK_1 , TEST_DATE_1, true);
        taskListPage.addTask(TEST_TASK_2, TEST_DATE_2, false);
    }

    /**
     * Tests if number of tasks is the same as the number of tasks created.
     */
    @Test
    public void testItemEntry() {
        // Arranging test members
        int expected = 2;

        // Acting on the test members //*[@id="list"]/ul
        list = (ArrayList<WebElement>) driver.findElements(By.cssSelector("div > ul > li"));
        int listSize = list.size();

        // Assert
        Assert.assertEquals(expected, listSize);
    }

    /**
     * Tests task name to be the same as the one created.
     */
    @Test
    public void testName() {

        // Arranging test members
        String name = TEST_TASK_1;
        list = (ArrayList<WebElement>) driver.findElements(By.xpath("//h3[contains(text(), '" + name + "')]"));

        // Acting on the test members

        String actual = driver.findElement(By.xpath("//h3[contains(text(), '" + name + "')]")).getText();

        //Assert
        Assert.assertEquals(name, actual);

    }

    /**
     * Tests task date to be the same as the one created.
     */
    @Test
    public void testDate() {

        // Arranging test members
        String expected = "Due By: " + TEST_DATE_1;

        // Acting on the test members
        String actual = driver.findElement(By.xpath("//ul/li[1]/p[contains(text(), '" + TEST_DATE_1 + "')]")).getText();

        //Assert
        Assert.assertEquals(expected, actual);
    }

    /**
     * Tests if completed task displays as completed.
     */
    @Test
    public void testComplete() {

        // Arranging test members
        String expected = "complete";

        // Acting on the test members
        String actual = driver.findElement(By.xpath("//ul/li[1]/span/button[contains(text(), '" + "complete" + "')]")).getText();

        //Assert
        Assert.assertEquals(expected, actual);
    }

    /**
     * Closes the Chrome Driver.
     */
    @After
    public void closeBrowser(){
        driver.quit();
    }

}
