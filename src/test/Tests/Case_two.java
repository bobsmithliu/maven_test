//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//
///**
// * @author Gurjot Sandher
// * @year 2021
// * @package PACKAGE_NAME
// */
//public class Case_two {
//
//    public static void main(String[] args) throws InterruptedException {
//
//        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\SeleniumDrivers\\chromedriver.exe");
//
//        WebDriver driver = new ChromeDriver();
//
//        driver.get("http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html");
//
//        TaskListPage taskListPage = new TaskListPage(driver);
//
//
//        taskListPage.enterTaskName("Test1");
////            System.out.println("Task Name has been entered.");
//
//        taskListPage.enterDeadlineInput("01/" + 02 + "/2015");
////            System.out.println("Test Date has been entered.");
//
//        taskListPage.clickBtn(taskListPage.completeCheckBox);
////            System.out.println("Box is checked.");
//
//        taskListPage.clickBtn(taskListPage.addItemBtn);
////        System.out.println("Item has been added.");
//
//        Thread.sleep(2000);
//
//        taskListPage.enterTaskName("Test2");
////            System.out.println("Task Name has been entered.");
//
//        taskListPage.enterDeadlineInput("01/" + 03 + "/2015");
////            System.out.println("Test Date has been entered.");
//
//        taskListPage.clickBtn(taskListPage.completeCheckBox);
////            System.out.println("Box is checked.");
//
//        taskListPage.clickBtn(taskListPage.addItemBtn);
////        System.out.println("Item has been added.");
//
//        Thread.sleep(2000);
//
////        taskListPage.isElementPresent();
//
////        taskListPage.createLists();
//
//        System.out.println(taskListPage.correctEntryCheck("name", "Test1"));
//
//        // This test shows as false incorrectly for some reason and must be corrected.
//        System.out.println(taskListPage.correctEntryCheck("name", "Test2"));
//
////        System.out.println("There should be an element present");
//    }
//}
