//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import java.util.Date;
//
///**
// * @author Gurjot Sandher
// * @year 2021
// * @package PACKAGE_NAME
// */
//public class Case_one {
//    /*
//        org.openqa.selenium.NoSuchElementException only works in this format
//     */
//    public static void main(String[] args) throws InterruptedException {
//        boolean present = false;
//
//        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\SeleniumDrivers\\chromedriver.exe");
//
//        WebDriver driver = new ChromeDriver();
//        long startTime;
//
////        driver.get("http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html");
//
////        System.out.println("Is the Fortinet Link displayed?");
//
//        try{
//            driver.get("http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html");
//
//            startTime = System.currentTimeMillis();
//
//            TaskListPage taskListPage = new TaskListPage(driver);
//
//            driver.findElement(taskListPage.taskPageHomeLink);
//            driver.findElement(taskListPage.title);
////            driver.findElement(By.xpath("//*[@id=\"list\"]/ul/li"));
//            driver.findElement(taskListPage.taskNameInput);
//            driver.findElement(taskListPage.deadlineInput);
//            driver.findElement(taskListPage.completeCheckBox);
//            driver.findElement(taskListPage.addItemBtn);
//            present = true;
//            long diff = System.currentTimeMillis() - startTime;
//            System.out.println("Page loaded in " + diff + " milliseconds.");
//
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            driver.quit();
//        }
//
//
//        try {
//            driver.findElement(taskListPage.taskPageHomeLink);
//            present = true;
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//        }
//
//        System.out.println("Is the Task List title displayed?");
//
//        try {
//            driver.findElement(taskListPage.title);
//            present = true;
//
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//        }
//
//        System.out.println("False Test");
//
//        // I am attempting to find the first element of the list here without adding any elements.
//        try {
//            driver.findElement(By.xpath("//*[@id=\"list\"]/ul/li"));
//            present = true;
//
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//        }
//
//        System.out.println("Is the Task Name text field displayed?");
//
//        try {
//            driver.findElement(taskListPage.taskNameInput);
//            present = true;
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//
//        }
//
//        System.out.println("Is the Deadline Selection field displayed?");
//
//        try {
//            driver.findElement(taskListPage.deadlineInput);
//            present = true;
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//
//        }
//
//        System.out.println("Is the Complete checkbox displayed?");
//
//        try {
//            driver.findElement(taskListPage.completeCheckBox);
//            present = true;
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//
//        }
//
//        System.out.println("Is the Add Item Button displayed?");
//
//        try {
//            driver.findElement(taskListPage.addItemBtn);
//            present = true;
//        } catch (org.openqa.selenium.NoSuchElementException e) {
//            present = false;
//        } finally {
//            System.out.println("Is present? " + present + "\n");
//            present = false;
//        }
//
//
//
//    }
//
//}
//
