/**
 * @author Zichang Liu
 * @year 2021
 * @package PACKAGE_NAME
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class Case_three {
    public static void main(String[] args) {
        //Initialize webdriver
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\SeleniumDrivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://s3.amazonaws.com/istreet-assets/bHFRMn4JHQnwP7QcqCer7w/fortinet-qa-testsite.html");
        TaskListPage testTLP = new TaskListPage(driver);

        //Setting up first task
        testTLP.enterTaskName("Test_Task1");
        testTLP.enterDeadlineInput("01/01/2022");
//        testTLP.clickBtn();

        //Setting up second task
        testTLP.enterTaskName("Test_Task2");
        testTLP.enterDeadlineInput("01/01/2022");
//        testTLP.clickCheckBox();
//        testTLP.clickAddItem();



    }

}
