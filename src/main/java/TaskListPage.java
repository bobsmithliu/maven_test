import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gurjot Sandher, Gurjot Mander
 * @year 2021
 * @package src/main/java
 *
 * This is a helper class for test case 1, 2, and 3.
 */

public class TaskListPage {

    /**  Constant for Chrome Driver. */
    WebDriver driver;

    /** Array list to store task names.  */
    ArrayList<String> listOfNames;

    /** Array list to store task dates.  */
    ArrayList<String> listOfDates;

    // LOCATORS

    // Locator for the Fortinet Link
    By taskPageHomeLink = By.xpath("//*[@id=\"header\"]/div/div/a");

    // Locator for the Task List title
    By title = By.xpath("//*[@id=\"body\"]/div/h1");

    // Locator for the Task Name input
    By taskNameInput= By.xpath("//*[@id=\"add\"]/form/div[1]/input");

    //Locator for the Deadline input
    By deadlineInput = By.xpath("/html/body/div[2]/div/div[1]/form/div[2]/input");

    // Locator for the Complete checkbox
    By completeCheckBox = By.xpath("/html/body/div[2]/div/div[1]/form/div[3]/input");

    //Locator for the AddItem button
    By addItemBtn = By.xpath("//*[@id=\"add-item\"]");

    /**
     * Constructor for TaskListPage.
     * @param driver WebDriver.
     */
    public TaskListPage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Getter for the title.
     * @return String.
     */
    public String getTitle(){
        return driver.getTitle();
    }

    /**
     * Method enters a given task name.
     * @param task String.
     */
    public void enterTaskName(String task){
        driver.findElement(taskNameInput).clear();
        driver.findElement(taskNameInput).sendKeys(task);
    }

    /**
     * Method enters a given deadline date.
     * @param datee String.
     */
    public void enterDeadlineInput(String datee){
        driver.findElement(deadlineInput).clear();
        driver.findElement(deadlineInput).sendKeys(datee);

        // I was not able to find a way to close the date picker after I entered the String.
        // This is a string for the Complete label of the checkbox to close the datepicker.
        driver.findElement(By.xpath("//*[@id=\"add\"]/form/div[3]/label")).click();
    }

    /**
     *  Method clicks a button on the screen.
     * @param btn taskListPage class' locators.
     */
    public void clickBtn(By btn) {
        driver.findElement(btn).click();
    }

    /**
     * Check if the name, deadline, or complete match the passing criteria
     * @param command String.
     * @param field String.
     * @return boolean.
     */
    public boolean correctEntryCheck(String command, String field){

        boolean displayed = false;

        if(command.equalsIgnoreCase("NAME")){

            displayed = listOfNames.contains(field);

        }

        if(command.equalsIgnoreCase("DUE BY")){
            displayed = listOfDates.contains(field);
        }

        return displayed;
    }

    /**
     * Method adds a task to the task list.
     * @param name string.
     * @param date string.
     * @param completion boolean.
     */
    public void addTask(String name, String date, boolean completion) {
        List<WebElement> list = driver.findElements(By.cssSelector("div > ul"));
//        ArrayList<String> listOfNames = new ArrayList<String>();
//        ArrayList<String> listOfDates = new ArrayList<String>();

        this.enterTaskName(name);

        this.enterDeadlineInput(date);
        
        if(completion){
            //Checks off the box
            this.clickBtn(this.completeCheckBox);
        }

        this.clickBtn(this.addItemBtn);

        new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(), '" + name + "')]")));

        // If box was checked this unchecks it after item has been entered.
        if(completion) {
            this.clickBtn(this.completeCheckBox);
        }

    }

}
